//
//  ViewController.m
//  Score Card
//
//  Created by click labs 127 on 9/7/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
UILabel *marks;
UILabel *maths;
UILabel *science;
UILabel *english;
UILabel *hindi;
UILabel *computer;
UILabel *percent;
UILabel *grade;
UITextField *mathstextfield;
UITextField *sciencetextfield;
UITextField *englishtextfield;
UITextField *hinditextfield;
UITextField *computertextfield;
UIButton *percentbutton;
UIButton *gradebutton;
NSInteger percentage_value;
UIImageView *imgview;

- (void)viewDidLoad {
    [super viewDidLoad];
    //wallpaper
    UIImageView *imgview1 = [UIImageView new];
    imgview1 .frame = CGRectMake(7, 9, 305, 550);
    imgview1 .image = [UIImage imageNamed:@"phone-5-wallpapers-0003.jpg"];
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"phone-5-wallpapers-0003.jpg"]];
    //marks label
    marks=[[UILabel alloc]init];
    marks.frame=CGRectMake(130,100,100,90);
    marks.text=@"Marks";
    marks.textColor=[UIColor whiteColor];
    [marks setFont:[UIFont boldSystemFontOfSize:20]];
    [self.view addSubview:marks];
    //maths label
    maths=[[UILabel alloc]init];
    maths.frame=CGRectMake(30,180,90,50);
    maths.text=@"Maths";
    maths.textColor=[UIColor whiteColor];
    [maths setFont:[UIFont boldSystemFontOfSize:20]];
    [self.view addSubview:maths];
    //science label
    science=[[UILabel alloc]init];
    science.frame=CGRectMake(30,230,90,50);
    science.text=@"Science";
    science.textColor=[UIColor whiteColor];
    [science setFont:[UIFont boldSystemFontOfSize:20]];
    [self.view addSubview:science];
    //english label
    english=[[UILabel alloc]init];
    english.frame=CGRectMake(30,280,90,50);
    english.text=@"English";
    english.textColor=[UIColor whiteColor];
    [english setFont:[UIFont boldSystemFontOfSize:20]];
    [self.view addSubview:english];
    //hindi label
    hindi=[[UILabel alloc]init];
    hindi.frame=CGRectMake(30,330,90,50);
    hindi.text=@"hindi";
    hindi.textColor=[UIColor whiteColor];
    [hindi setFont:[UIFont boldSystemFontOfSize:20]];
    [self.view addSubview:hindi];
    //computer label
    computer=[[UILabel alloc]init];
    computer.frame=CGRectMake(30,380,130,50);
    computer.text=@"Computers";
    computer.textColor=[UIColor whiteColor];
    [computer setFont:[UIFont boldSystemFontOfSize:20]];
    [self.view addSubview:computer];
    //maths textfield
    mathstextfield=[[UITextField alloc]init];
    mathstextfield.placeholder=@"Enter maths marks";
    mathstextfield.frame=CGRectMake(150,190,150, 30);
    mathstextfield.textColor=[UIColor blackColor];
    mathstextfield.backgroundColor=[UIColor whiteColor];
    mathstextfield.borderStyle=UITextBorderStyleRoundedRect;
    [mathstextfield setFont:[UIFont boldSystemFontOfSize:12]];
    [self.view addSubview:mathstextfield];
    //science textfiled
    sciencetextfield=[[UITextField alloc]init];
    sciencetextfield.placeholder=@"Enter science marks";
    sciencetextfield.frame=CGRectMake(150,240,150, 30);
    sciencetextfield.textColor=[UIColor blackColor];
    sciencetextfield.backgroundColor=[UIColor whiteColor];
    sciencetextfield.borderStyle=UITextBorderStyleRoundedRect;
    [sciencetextfield setFont:[UIFont boldSystemFontOfSize:12]];
    [self.view addSubview:sciencetextfield];
    //english textfield
    englishtextfield=[[UITextField alloc]init];
    englishtextfield.placeholder=@"Enter english marks";
    englishtextfield.frame=CGRectMake(150,290,150, 30);
    englishtextfield.textColor=[UIColor blackColor];
    englishtextfield.backgroundColor=[UIColor whiteColor];
    englishtextfield.borderStyle=UITextBorderStyleRoundedRect;
    [englishtextfield setFont:[UIFont boldSystemFontOfSize:12]];
    [self.view addSubview:englishtextfield];
    //hindi textfield
    hinditextfield=[[UITextField alloc]init];
    hinditextfield.placeholder=@"Enter hindi marks";
    hinditextfield.frame=CGRectMake(150,340,150, 30);
    hinditextfield.textColor=[UIColor blackColor];
    hinditextfield.backgroundColor=[UIColor whiteColor];
    hinditextfield.borderStyle=UITextBorderStyleRoundedRect;
    [hinditextfield setFont:[UIFont boldSystemFontOfSize:12]];
    [self.view addSubview:hinditextfield];
    //computer textfiled
    computertextfield=[[UITextField alloc]init];
    computertextfield.placeholder=@"Enter computer marks";
    computertextfield.frame=CGRectMake(150,390,150, 30);
    computertextfield.textColor=[UIColor blackColor];
    computertextfield.backgroundColor=[UIColor whiteColor];
    computertextfield.borderStyle=UITextBorderStyleRoundedRect;
    [computertextfield setFont:[UIFont boldSystemFontOfSize:12]];
    [self.view addSubview:computertextfield];
    //percentage button
    percentbutton=[[UIButton alloc]init];
    percentbutton.frame=CGRectMake(170,450,120, 30);
    [percentbutton setTitle:@"Percentage" forState:UIControlStateNormal];
    percentbutton.backgroundColor=[UIColor grayColor];
    [percentbutton addTarget:self action:@selector(percentage:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:percentbutton];
    //grade button
    gradebutton=[[UIButton alloc]init];
    gradebutton.frame=CGRectMake(50,450, 90, 30);
    [gradebutton setTitle:@"Grade" forState:UIControlStateNormal];
    gradebutton.backgroundColor=[UIColor grayColor];
    [gradebutton addTarget:self action:@selector(grade:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:gradebutton];
    //PERCENT LABEL
    percent=[[UILabel alloc]initWithFrame:CGRectZero];
    percent.textColor=[UIColor whiteColor];
    [percent setFont:[UIFont boldSystemFontOfSize:20]];
    percent.frame=CGRectMake(50,490,180,40);
    [self.view addSubview:percent];
    //GRADE LABEL
    grade=[[UILabel alloc]initWithFrame:CGRectZero];
    [grade setFont:[UIFont boldSystemFontOfSize:20]];
    grade.textColor=[UIColor whiteColor];
    grade.frame=CGRectMake(50,520,150,40);
    [self.view addSubview:grade];
    //Inserting Image
    imgview=[[UIImageView alloc]init];
    imgview.frame=CGRectMake(90,30,130, 90);
    imgview.image=[UIImage imageNamed:@"Results-Button.png"];
    [self.view addSubview:imgview];
    
    



    // Do any additional setup after loading the view, typically from a nib.
}
-(void) percentage:(UIButton *)sender
{
    NSInteger sum;
    NSString *m=mathstextfield.text;
        NSString *s=sciencetextfield.text;
        NSString *e=englishtextfield.text;
        NSString *h=hinditextfield.text;
        NSString *c=computertextfield.text;

    if ([self checkCharacter: mathstextfield.text] == YES){
        
        [self alert1];
        imgview.image=[UIImage imageNamed:@"invalid-stamp.gif"];
        
    }
    
    else if ([self checkCharacter: sciencetextfield.text] == YES){
        
        [self alert2];
        imgview.image=[UIImage imageNamed:@"invalid-stamp.gif"];
    }
    
    else if ([self checkCharacter: englishtextfield.text] == YES){
        
        [self alert3];
        imgview.image=[UIImage imageNamed:@"invalid-stamp.gif"];
        
    }
    
    else if ([self checkCharacter: hinditextfield.text] == YES){
        
        [self alert4];
        imgview.image=[UIImage imageNamed:@"invalid-stamp.gif"];
        
    }
    
    else if ([self checkCharacter: computertextfield.text] == YES){
        
        [self alert5];
        imgview.image=[UIImage imageNamed:@"invalid-stamp.gif"];
        
    }
        else if(m.integerValue<=100&&s.integerValue<=100&&e.integerValue<=100&&h.integerValue<=100&&c.integerValue<=100)
        {
            sum=(m.integerValue+s.integerValue+e.integerValue+h.integerValue+c.integerValue);
            percentage_value=(sum*100)/500;
            percent.text=[NSString stringWithFormat:@"Percentage is : %ld",percentage_value];
        }
        else
        {
            percent.text=@"not valid";
        }
}

-(BOOL)checkCharacter:(NSString*)text{
    NSCharacterSet *c=[[NSCharacterSet characterSetWithCharactersInString:@"1234567890+-*/%"]invertedSet];
    if ([text rangeOfCharacterFromSet:c].location==NSNotFound) {
        NSLog(@"No Speacial Character");
    } else {
        NSLog(@"Speacial Character Found");
        return YES;
    }
    return NO;
}

-(void) grade:(UIButton *)sender
{
    if (percentage_value>80&&percentage_value<=100)
    {
       grade.text=@"Grade is : A ";
        imgview.image=[UIImage imageNamed:@"url.png"];

    }
    else if(percentage_value>70&&percentage_value<=80)
    {
        grade.text=@"Grade is : B";
        imgview.image=[UIImage imageNamed:@"B.0.png"];

    }
    else if(percentage_value>60&&percentage_value<=70)
    {
        grade.text=@"Grade is : C";
        imgview.image=[UIImage imageNamed:@"Images.jpg"];

    }
    else if(percentage_value>50&&percentage_value<=60)
    {
        grade.text=@"Grade is : D";
        imgview.image=[UIImage imageNamed:@"url.jpg"];

    }
    else if(percentage_value>40&&percentage_value<=50)
    {
        grade.text=@"Grade is : E";
        imgview.image=[UIImage imageNamed:@"river.jpg"];

    }
    else
    {
        grade.text=@"Grade is : F";
        imgview.image=[UIImage imageNamed:@"gradef.png"];

    }

}
-(void)alert1{
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Math:" message:@"Only Digits Are Allowed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert1 show];
    
}

-(void)alert2{
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Science" message:@"Only Digits Are Allowed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert1 show];
    
}

-(void)alert3{
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"English" message:@"Only Digits Are Allowed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert1 show];
    
}

-(void)alert4{
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Hindi" message:@"Only Digits Are Allowed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert1 show];
    
}

-(void)alert5{
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Computer" message:@"Only Digits Are Allowed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert1 show];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
